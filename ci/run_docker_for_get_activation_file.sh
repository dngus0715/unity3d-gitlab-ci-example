#!/usr/bin/env bash

DOCKER_IMAGE=$IMAGE:$UNITY_VERSION-base-$IMAGE_VERSION

echo $activation_file

docker run -i --rm \
	-e "UNITY_USERNAME=${UNITY_USERNAME}" \
	-e "UNITY_PASSWORD=${UNITY_PASSWORD}" \
	-e "TEST_PLATFORM=linux" \
	-e "WORKDIR=/root/project" \
	-v "$(pwd):/root/project" \
	$DOCKER_IMAGE \
	/bin/bash
bash 

