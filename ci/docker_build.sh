#!/usr/bin/env bash

set -e

docker run \
  -e "BUILD_NAME=test" \
  -e "UNITY_LICENSE=$UNITY_LICENSE" \
  -e "BUILD_TARGET=$BUILD_TARGET" \
  -e "UNITY_USERNAME=$UNITY_USERNAME" \
  -e "UNITY_PASSWORD=$UNITY_PASSWORD" \
  -w /project/ \
  -v $UNITY_DIR:/project/ \
  $DOCKER_IMAGE \
  /bin/bash -c "/project/ci/before_script.sh && /project/ci/build.sh"
